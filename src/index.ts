import "./styles.css"

let counter = 0;

const sources: NodeList = document.querySelectorAll('*[data-animation-source]')
const target: HTMLElement = document.querySelector('*[data-animation-target]')
const counterElement: HTMLElement = document.querySelector('#counter');

sources.forEach((source) => {
  source.addEventListener('click', (e: MouseEvent) => {
    const { clientX, clientY } = e;

    const newElement = document.createElement('div');

    newElement.className = 'animation-ball'
    newElement.style.position = 'fixed';
    newElement.style.top = clientY + 'px';
    newElement.style.left = clientX + 'px';

    document.body.append(newElement)

    animateToTarget(newElement);
  });
})


target.addEventListener('click', () => {
  resetCounter();
})

const animateToTarget = (element: HTMLElement) => {
  const { x: targetX, y: targetY, width: targetWidth, height: targetHeight} = target.getBoundingClientRect();
  const { x: elementX, y: elementY, width: elementWidth, height: elementHeight} = element.getBoundingClientRect();

  const offsetX = target.dataset.animationOffsetX
  const offsetY = target.dataset.animationOffsetY

  const endX = targetX + (targetWidth / 2) - (elementWidth / 2) + (offsetX ? Number(offsetX) : 0);
  const endY = targetY + (targetHeight / 2) - (elementHeight / 2) + (offsetY ? Number(offsetY) : 0);

  const startX = elementX  + (elementHeight / 2);
  const startY = elementY  + (elementHeight / 2);

  const distanceX = startX > endX ? startX - endX : (startX - endX) * -1;
  const distanceY = startY > endY ? (startY - endY) * -1 : startY - endY;

  element.addEventListener('transitionend', handleTransitionEnd(element));

  element.style.transform = `translate(${distanceX}px, ${distanceY}px)`
}

const handleTransitionEnd = (element: HTMLElement) => (event: TransitionEvent) => {
  if(event.propertyName === 'opacity') {
    element.parentNode.removeChild(element);
  } else {
    element.style.opacity = '0';
    counter++

    updateCounter()
  }
}

const updateCounter = () => {
  counterElement.innerHTML = String(counter);

  const opacity = window.getComputedStyle(counterElement).getPropertyValue("opacity")

  if(opacity === '0' && counter > 0) {
    counterElement.style.opacity = '1'
  }
}

const resetCounter = () => {
  counterElement.innerHTML = "";
  counterElement.style.opacity = '0'

  counter = 0;
}
